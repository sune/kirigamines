/*
 * Copyright (c) 2018-2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import QtTest 1.0
import "../src/qml/KirigaMinesUtils.js" as Utils

TestCase {
    name: "step"
    function test_step_small_board() {
        var board = Utils.createBoard(3, 3, 0);
        var alive = Utils.step(Qt.point(1,1),board);
        compare(alive, true, "should be alive");
	for (var i = 0 ; i < 3 ; i++ ) {
	   for(var j = 0 ; j < 3 ; j++) {
	      compare(board[i][j].bomb,false);
	      compare(board[i][j].revealed, true);
	      compare(board[i][j].neighborBombs,0);
	   }
	}
    }

    function test_step_small_board_simple_dead() {
        var board = Utils.createBoard(3, 3, 9);
        var alive = Utils.step(Qt.point(1,1),board);
        compare(alive, false, "should be dead");
    }
    function test_step_on_flag() {
        var board = Utils.createBoard(3, 3, 9);
        var alive = Utils.step(Qt.point(1,1),board,"flag");
        compare(alive, true, "flagged");
        alive = Utils.step(Qt.point(1,1),board);
        compare(alive, true, "stepped on flagged");
        alive = Utils.step(Qt.point(1,1),board,"flag");
        compare(alive, true, "unflagged");
        alive = Utils.step(Qt.point(1,1),board);
        compare(alive, false, "should be dead");
    }
    function test_step_more_complex8() {
        var board = Utils.createBoard(3, 3, 9);
        board[1][1].bomb = false;
        var alive = Utils.step(Qt.point(1,1),board)
        compare(alive, true, "board changed");
	for (var i = 0 ; i < 3 ; i++ ) {
	   for(var j = 0 ; j < 3 ; j++) {
               if (i === 1 && j == 1) {
                   continue;
               }
	      compare(board[i][j].bomb, true, "" + i + " " + j);
	      compare(board[i][j].revealed, false, "" + i + " " + j);
	   }
	}
	compare(board[1][1].bomb, false);
	compare(board[1][1].revealed, true);
	compare(board[1][1].neighborBombs, 8);
    }
    function test_step_more_complex16() {
        var board = Utils.createBoard(4, 4, 16);
	for (var i = 1 ; i < 3 ; i++ ) {
	   for(var j = 1 ; j < 3 ; j++) {
	      board[i][j].bomb = false;
	   }
	}
        var alive = Utils.step(Qt.point(2,2),board)
        compare(alive, true, "board changed");
	for (var i = 0 ; i < 4 ; i++ ) {
	   for(var j = 0 ; j < 4 ; j++) {
               if (i === 2 && j == 2) {
                   continue;
               }
	      compare(board[i][j].revealed, false, "" + i + " " + j);
	   }
	}
	compare(board[2][2].neighborBombs, 5, "woopsie")
	compare(board[2][2].revealed, true, "woopsie")
    }

    function test_step_more_complex25() {
        var board = Utils.createBoard(5, 5, 25);
	for (var i = 1 ; i < 4 ; i++ ) {
	   for(var j = 1 ; j < 4 ; j++) {
	      board[i][j].bomb = false;
	   }
	}
        var alive = Utils.step(Qt.point(2,2),board)
        compare(alive, true, "board changed");
	compare(board[0][0].revealed, false, "bug");
	compare(board[0][1].revealed, false, "bug");
	compare(board[0][2].revealed, false, "bug");
	compare(board[0][3].revealed, false, "bug");
	compare(board[0][4].revealed, false, "bug");
	compare(board[4][0].revealed, false, "bug");
	compare(board[4][1].revealed, false, "bug");
	compare(board[4][2].revealed, false, "bug");
	compare(board[4][3].revealed, false, "bug");
	compare(board[4][4].revealed, false, "bug");
	compare(board[1][0].revealed, false, "bug");
	compare(board[2][0].revealed, false, "bug");
	compare(board[3][0].revealed, false, "bug");
	compare(board[0][1].revealed, false, "bug");
	compare(board[0][2].revealed, false, "bug");
	compare(board[0][3].revealed, false, "bug");
	compare(board[0][4].revealed, false, "bug");
	for (var i = 1 ; i < 4 ; i++ ) {
	   for(var j = 1 ; j < 4 ; j++) {
	      compare(board[i][j].revealed, true, "missstep" +i + " " + j );
	   }
	}
	compare(board[2][2].neighborBombs, 0, "woopsie")
	compare(board[3][3].neighborBombs, 5, "woopsie")
	compare(board[1][3].neighborBombs, 5, "woopsie")
	compare(board[1][1].neighborBombs, 5, "woopsie")
	compare(board[3][1].neighborBombs, 5, "woopsie")
	compare(board[3][2].neighborBombs, 3, "woopsie")
	compare(board[2][3].neighborBombs, 3, "woopsie")
	compare(board[1][2].neighborBombs, 3, "woopsie")
	compare(board[2][1].neighborBombs, 3, "woopsie")
        compare(Utils.countBoard(board, Utils.isUnrevealed), 16)


    }

}

