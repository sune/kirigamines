/*
 * Copyright (c) 2018-2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import QtTest 1.0
import "../src/qml/KirigaMinesUtils.js" as Utils

TestCase {
    name: "createBoard"
    function onlyUnique(value, index, self) {
    	return self.indexOf(value) === index;
    }

    function test_bomb_positions1() {
        var pos = Utils.bombPositions(1,1);
	compare(pos.length, 1, "wrong count");
	compare(pos, [0], "wrong result");
    }
    function test_bomb_positions2() {
        var pos = Utils.bombPositions(10,100);
	compare(pos.length, 10, "wrong count");
	var uniquepos = pos.filter( onlyUnique )
	compare(uniquepos, pos, "");
    }
    function test_small_board() {
        var board = Utils.createBoard(1, 1, 1);
        compare(board.length, 1, "wrong one dimension");
        compare(board[0].length, 1, "wrong other dimension");
        compare(board[0][0].bomb, true, "wrong content");
    }
    function test_larger_square_board() {
        var board = Utils.createBoard(5, 5, 25);
        compare(board.length, 5, "wrong one dimension");
        for(var i = 0 ; i < board.length; i++) {
            compare(board[i].length, 5, "wrong other dimension");
            for(var j = 0 ; j < board[i].length; j++) {
                compare(board[i][j].bomb, true, "wrong content");
            }
        }
    }
    function test_rect_board() {
        var board = Utils.createBoard(5, 6, 30);
        compare(board.length, 6, "wrong one dimension");
        for(var i = 0 ; i < board.length; i++) {
            compare(board[i].length, 5, "wrong other dimension");
            for(var j = 0 ; j < board[i].length; j++) {
                compare(board[i][j].bomb, true, "wrong content");
            }
        }
    }
    function test_too_many_bombs() {
        var board = Utils.createBoard(1, 1, 3);
        compare(board, null, "no board can fulfill this");
    }
    function test_all_bombs() {
        var board = Utils.createBoard(5, 6, 30);
        compare(board === null, false, "no board can fulfill this");
    }
    function test_bomb_count() {
        var board = Utils.createBoard(5, 6, 15);
	var bombCounter = Utils.countBoard(board,Utils.isBomb);
	compare(bombCounter, 15, "wrong bomb count")
    }
    function test_unrevealed_count() {
        var board = Utils.createBoard(3,3,0)
        board[1][1].revealed = true;
        board[0][0].revealed = true;
        board[2][2].revealed = true;
        var revealedCounter = Utils.countBoard(board,Utils.isUnrevealed);
        compare(revealedCounter, 6, "wrong revealed")
    }
}
