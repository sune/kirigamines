/*
 * Copyright (c) 2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "basicpausabletimer.h"


BasicPausableTimer::BasicPausableTimer() : m_counter {0} {}

bool BasicPausableTimer::isRunning() const
{
    return m_elapsed.isValid();
}

std::chrono::milliseconds BasicPausableTimer::value() const
{
    return m_counter + std::chrono::milliseconds(m_elapsed.isValid() ? m_elapsed.elapsed() : 0 );

}

void BasicPausableTimer::reset()
{
    m_elapsed.invalidate();
    m_counter = {};
}

void BasicPausableTimer::setRunning(bool running)
{
    if (running != isRunning()) {
        if (!running) {
            if (m_elapsed.isValid()) {
                m_counter += std::chrono::milliseconds(m_elapsed.elapsed());
            }
            m_elapsed.invalidate();
        }
        else {
            m_elapsed.restart();
        }
    }
}
