/*
 * Copyright (c) 2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "levels.h"

QVariantList Levels::createLevels() {
    QVariantList list ;
    list.push_back(QVariant::fromValue(Level{tr("Beginner"), "beginner", 10, 10, 10}));
    list.push_back(QVariant::fromValue(Level{tr("Intermediate"), "intermediate", 16, 16, 40}));
    list.push_back(QVariant::fromValue(Level{tr("Advanced"), "advanced", 16, 30, 99}));
    list.push_back(QVariant::fromValue(Level{tr("Expert"), "expert", 24, 30, 200}));
    return list;
}

QVariantList Levels::levels() {
    static QVariantList list = createLevels();
    return list;
}

QString Levels::initialLevelId() const
{
    return levels().first().value<Level>().id();
}
