/*
 * Copyright (c) 2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#pragma once

#include <QObject>
#include <QTimer>
#include "basicpausabletimer.h"

class PausableTimer : public QObject {
    Q_OBJECT
    Q_PROPERTY(int value READ value NOTIFY valueChanged)
    Q_PROPERTY(int updateRate READ updateRate WRITE setUpdateRate NOTIFY updateRateChanged)
    Q_PROPERTY(bool running READ isRunning WRITE setRunning NOTIFY runningChanged)
public:
    PausableTimer(QObject* parent = nullptr);
    Q_INVOKABLE inline void restart() {
        reset();
        start();
    }
    Q_INVOKABLE inline void start() {
        setRunning(true);
    }
    Q_INVOKABLE void reset();
    Q_INVOKABLE inline void stop() {
        setRunning(false);
    }
    bool isRunning() const;
    void setRunning(bool running);
    int updateRate() const;
    void setUpdateRate(int updateRate);
    int value() const ;
Q_SIGNALS:
    void runningChanged();
    void valueChanged();
    void updateRateChanged();
private:
    QTimer m_refreshTimer;
    BasicPausableTimer m_elapsedTimer;
};
