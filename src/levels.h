/*
 * Copyright (c) 2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <QObject>
#include <QVariant>

class Level {
    Q_GADGET
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(int columns READ columns CONSTANT)
    Q_PROPERTY(int rows READ rows CONSTANT)
    Q_PROPERTY(int bombs READ bombs CONSTANT)
  public:
    Level(QString name, QString id, int rows, int columns, int bombs) : m_name(name), m_id(id), m_columns(columns), m_rows(rows), m_bombs(bombs) {}
    Level() = default;
    QString name() const { return m_name; }
    QString id() const { return m_id; }
    int columns() const { return m_columns; }
    int rows() const { return m_rows; }
    int bombs() const { return m_bombs; }
  private:
    QString m_name;
    QString m_id;
    int m_columns;
    int m_rows;
    int m_bombs;
};
Q_DECLARE_METATYPE(Level);

class Levels : public QObject {
  Q_OBJECT
  Q_PROPERTY(QVariantList levels READ levels CONSTANT);
  Q_PROPERTY(QString initialLevelId READ initialLevelId CONSTANT);
  public:
    Q_INVOKABLE static QVariantList levels();
    Q_INVOKABLE QString initialLevelId() const;

  private:
    static QVariantList createLevels();

};

class QQmlEngine;
class QJSEngine;

inline QObject* levelsSingleton(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine)
    return new Levels;
}
