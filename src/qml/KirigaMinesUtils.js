/*
 * Copyright (c) 2018-2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


// gather the neighbor points from a given point, if they exist within the board
// helper function for 'fill'
function gatherNeighbors(point, xmax, ymax) {
    var n = []
    if (point.x > 0) {
        n.push(Qt.point(point.x - 1, point.y))
    }
    if (point.x < xmax) {
        n.push(Qt.point(point.x + 1, point.y))
    }
    if (point.y > 0) {
        n.push(Qt.point(point.x, point.y - 1))
    }
    if (point.y < ymax) {
        n.push(Qt.point(point.x, point.y + 1))
    }
    if (point.x > 0 && point.y > 0) {
        n.push(Qt.point(point.x - 1, point.y - 1))
    }
    if (point.x < xmax && point.y > 0) {
        n.push(Qt.point(point.x + 1, point.y - 1))
    }
    if (point.x > 0 && point.y < ymax) {
        n.push(Qt.point(point.x - 1, point.y + 1))
    }
    if (point.x < xmax && point.y < ymax) {
        n.push(Qt.point(point.x + 1, point.y + 1))
    }
    return n
}

function getLocation(point, board) {
    return board[point.x][point.y]
}

function isFlagged(item) {
    return item.flag
}
function isBomb(item) {
    return item.bomb
}
function isUnrevealed(item) {
    return !item.revealed && !item.bomb
}

function countList( list, board, countFunction) {
  var count = 0;
  for(var i = 0 ; i < list.length; i++) {
    var point = list[i]
    if (countFunction(getLocation(point, board))) {
      count++
    }
  }
  return count
}
function countRow( row, board, countFunction) {
  var count = 0;
  for(var i = 0 ; i < row.length; i++) {
    if (countFunction(row[i], board)) {
      count++
    }
  }
  return count
}

function countBoard(board, countFunction) {
    var count = 0
    for(var i = 0; i < board.length ; i++) {
        count = count + countRow(board[i] , board, countFunction)
    }
    return count
}

// steps on a point
function step(point,board,type) {
    var currentLoc = getLocation(point,board)
    if (currentLoc.revealed) {
       return true;
    }
    if (type === "flag" ) {
        currentLoc.flag = !currentLoc.flag;
        return true;
    }
    if (currentLoc.flag) {
        return true;
    }
    if (currentLoc.bomb) {
      currentLoc.revealed = true;
      return false;
    }
    var frontier = [point]
    var seen = []
    while (frontier.length > 0 ) {
        var current = frontier.pop()
	currentLoc = getLocation(current, board)
        seen.push(current)
	if (currentLoc.bomb) {
	  continue;
	}
        var neighbors = gatherNeighbors(current, board.length -1, board[0].length -1)
	if (currentLoc.neighborBombs === -1) {
	  currentLoc.neighborBombs = countList(neighbors, board, isBomb)
	}
	currentLoc.revealed = true;
	if (currentLoc.neighborBombs !== 0) {
	  continue;
	}
        while (neighbors.length > 0) {
            var neighbor = neighbors.pop()
            var found = false
            for(var i = 0 ; i < seen.length; i++) {
                if (seen[i].x === neighbor.x && seen[i].y === neighbor.y) {
                    found = true
                    break
                }
            }
            if (!found) {
                for(var i = 0 ; i < frontier.length; i++) {
                    if (frontier[i].x === neighbor.x && frontier[i].y === neighbor.y) {
                        found = true
                        break
                    }
                }
            }
            if (!found) {
                var neighborLoc = getLocation(neighbor,board)
                if (!neighborLoc.flag)
                    frontier.push(neighbor)
            }
        }
    }
    return true;
}

function bombPositions(bombs, maxcount) {
    var bombPlaces = []
    while(bombPlaces.length < bombs){
        var r = Math.floor(Math.random()*maxcount);
        if(bombPlaces.indexOf(r) === -1) {
	  bombPlaces.push(r);
	}
    }
    return bombPlaces;
}

// initializes a board with height, width and bombs
function createBoard(height, width, bombs ) {
    if (bombs > (height * width)) {
       return null;
    }
    var bombPlaces = bombPositions(bombs, height * width);
    var board = []
    for(var i = 0 ; i < width ; i++) {
        var lane = []
        for(var j = 0 ; j < height ; j++) {
            lane.push({revealed: false, bomb: (bombPlaces.indexOf(i*height + j) !== -1), neighborBombs: -1, flag: false})
        }
        board.push(lane)
    }
    return board
}
