import QtQuick 2.0
import QtQuick.Controls 2.0
Label {
    text: "Reveal all mines as fast as possible. A flag prevents stepping on a cell. The number tells how many mines are adjacant to that cell. Win condition is not to place as many flags as there are mines, but to reveal all fields that aren't mines"
    wrapMode: TextArea.Wrap
}
