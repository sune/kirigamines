import QtQuick 2.0
import QtQuick.Controls 2.0
import "KirigaMinesUtils.js" as Utils
import org.kde.kirigami 2.0 as Kirigami
import kirigamines 1.0

Kirigami.Page {
    property bool alive: true
    property bool winner: false
    property bool started: false;
    property var board: []
    property int flagCount;
    property int elapsed: timer.value
    enabled: alive && !winner
    property var level;
    onLevelChanged: {
        restart()
    }
    function restart() {
        board = Utils.createBoard(level.rows, level.columns, level.bombs)
        alive = true;
        winner = false;
        timer.reset();
        started = false;
        flagCount = 0;
        canvas.requestPaint()
    }
    function colorForBombCount(count) {
        switch(count) {
            case 1: return "blue"
            case 2: return "green"
            case 3: return "red"
            case 4: return "navy"
            case 5: return "maroon"
            case 6: return "purple"
            case 7: return "olive"
            case 8: return "teal"

        }
        return "black"
    }
    function colorForCell(item) {
        if (item.revealed) {
            if (item.bomb) {
                return "red"
            } else {
                return "white"
            }
        }
        if (enabled) {
            if (item.flag) {
                return "blue"
            } else {
                return "grey"
            }
        }
        else {
            if (item.bomb && item.flag)
            {
                return "blue"
            } else if (item.flag) {
                return "purple"
            }
            if (item.bomb) {
                return "pink"
            }
            return "grey"
        }

    }
    PausableTimer {
        running: started && enabled && !pauseButton.checked
        id: timer
    }
    Canvas {
        anchors.fill: parent
        anchors.margins: 20
        anchors.bottomMargin: 120
        anchors.topMargin: 40
        id: canvas;
        visible: !pauseButton.checked
        onPaint: {
            var ctx = getContext("2d")
            ctx.fillRect(0,0,canvasSize.width, canvasSize.height)
            ctx.textBaseline= "top"
            ctx.textAlign = "center"
            var number = board.length
            var fieldheight = canvasSize.height / board[0].length
            var fieldwidth = canvasSize.width / number
            ctx.font = (fieldheight*.8)+"px sans-serif"
            for(var i = 0; i < number ; i++) {
                for(var j = 0 ; j < board[0].length ; j++) {
                    var item = Utils.getLocation(Qt.point(i,j),board)
                    ctx.fillStyle = colorForCell(item);
                    ctx.fillRect(Math.ceil(i*fieldwidth), Math.ceil(j* fieldheight), Math.ceil(fieldwidth), Math.ceil( fieldheight))
                    ctx.fillStyle = "black"
                    ctx.strokeRect(Math.ceil(i*fieldwidth), Math.ceil(j* fieldheight), Math.ceil(fieldwidth), Math.ceil( fieldheight))

                    if (item.revealed) {
                        var ncount = item.neighborBombs;
                        if (ncount > 0) {
                        ctx.fillStyle = colorForBombCount(ncount)
                            ctx.fillText(ncount, Math.ceil(i*fieldwidth) + Math.ceil(fieldwidth)/2, Math.ceil(j* fieldheight) + fieldheight*.05)
                        }
                    }
                }
            }
        }
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.RightButton | Qt.LeftButton | Qt.MiddleButton
            onClicked: {
                if (!started) {
                    started = true
                }
                var fieldwidth = canvas.canvasSize.width / board.length
                var fieldX = Math.floor(mouse.x / fieldwidth);
                var fieldHeight = canvas.canvasSize.height / board[0].length
                var fieldY = Math.floor(mouse.y / fieldHeight);
                var location = Qt.point(fieldX,fieldY);
                var stepResult = true
                switch(mouse.button) {
                    case Qt.LeftButton:
                    case Qt.RightButton: {
                        var steptype = (mouse.button == Qt.RightButton || flagButton.checked) ? "flag" : ""
                        stepResult = stepResult && Utils.step(location, board, steptype)
                        break;
                    }
                    case Qt.MiddleButton: {
                        var spots = Utils.gatherNeighbors(location, board.length -1, board[0].length -1);
                        for (var i = 0; i < spots.length; i++) {
                            stepResult = stepResult&&Utils.step(spots[i], board, null)
                        }
                        break;
                    }
                }

                canvas.requestPaint()
                var unrevealed = Utils.countBoard(board, Utils.isUnrevealed)
                flagCount = Utils.countBoard(board, Utils.isFlagged)
                alive = stepResult;
                winner = stepResult && (unrevealed === 0)
            }
        }
    }

    Item {
        anchors.bottom: parent.bottom
        height: 100
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 20
        Item {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            width: parent.width*.4
            id:flagButton
            property bool checked: false
            MouseArea {
                anchors.fill: parent
                onClicked: parent.checked = !parent.checked
            }
            Rectangle {
                anchors.fill: parent
                anchors.margins: 5
                color: parent.checked ? "blue" : "transparent"
                border.color: parent.checked ? "white" : "blue"
                border.width: 5
                Kirigami.Heading {
                    color: parent.parent.checked ? "white" : "blue"
                    anchors.centerIn: parent
                    text: "Flag"
                }
            }
        }
        Item {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: parent.width*.4
            id:pauseButton
            property bool checked: false
            MouseArea {
                anchors.fill: parent
                onClicked: parent.checked = !parent.checked
            }
            Rectangle {
                anchors.fill: parent
                anchors.margins: 5
                color: parent.checked ? "blue" : "transparent"
                border.color: parent.checked ? "white" : "blue"
                border.width: 5
                Kirigami.Heading {
                    color: parent.parent.checked ? "white" : "blue"
                    anchors.centerIn: parent
                    text: "Pause"
                }
            }
        }
    }

}
