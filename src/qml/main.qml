/*
 * Copyright (c) 2018-2019 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0 as Controls
//import QtQuick.Layouts 1.2
import org.kde.kirigami 2.4 as Kirigami
import kirigamines 1.0

Kirigami.ApplicationWindow
{
    visible: true
    width: 480
    height: 640
    title: "KirigaMines"
    header: Item {
        height: childrenRect.height
        anchors.left: parent.left
        Kirigami.Heading {
            text: "Seconds: " + gamearea.elapsed
        }
        Kirigami.Heading {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Left: "  + (gamearea.level.bombs - gamearea.flagCount);
        }
        Kirigami.Heading {
            anchors.right: parent.right
            text: "Bombs: " + gamearea.level.bombs ;
        }

    }

    HighScore {
        id: highscorehandler
        level: gamearea.level.id
    }

    globalDrawer: Kirigami.GlobalDrawer {
        title: "KirigaMines"
        titleIcon: "games-mines"
        actions: [
            Kirigami.Action {
                text: "Restart"
                iconName: "view-refresh"
                onTriggered:gamearea.restart()
            },
            Kirigami.Action {
                text: "Highscore"
                iconName: "games-highscores"
                onTriggered: highscore.sheetOpen = true
            },
            Kirigami.Action {
                text: "Help"
                iconName: "help-contents"
                onTriggered: help.sheetOpen = true
            }
        ]
        Controls.ButtonGroup {
            exclusive: true
            id: levelGroup
            property var level: checkedButton.level
            onLevelChanged: gamearea.level = level
        }
        Repeater {
            model: Levels.levels
            Controls.CheckBox {
                checked: modelData.id === Levels.initialLevelId
                Controls.ButtonGroup.group: levelGroup
                text:  modelData.name
                property var level: modelData
            }
        }
    }

    Kirigami.OverlaySheet {
        id: help
        header: Kirigami.Heading { text: "Help" }
        Help {
        }
    }

    Kirigami.OverlaySheet {
        id: highscore
        header: Kirigami.Heading {
            text: "High score"
        }
        HighScoreView {
             allHighscores: highscorehandler.allHighscores

        }

    }
    Kirigami.OverlaySheet {
        id: win
        header: Kirigami.Heading {
            text: "Winner!"
        }
        Winner {
            id: winBanner
            property alias highscorebeaten: winBanner.highscoreBeaten;
            timeUsed: gamearea.elapsed
            onRestartClicked: {
                gamearea.restart()
                win.sheetOpen = false
            }
        }
    }
    Kirigami.OverlaySheet {
        id: loser
        header: Kirigami.Heading {
            text: "Bomb!"
        }
        Loser {
            id: loseBanner
            onRestartClicked: {
                gamearea.restart()
                loser.sheetOpen = false
            }
        }
    }

    pageStack.initialPage: GameArea {
        title: levelGroup.checkedButton.text
        topPadding: 0
        bottomPadding: 0
        leftPadding: 0
        rightPadding: 0
        id: gamearea
        anchors.bottomMargin: 40
        onAliveChanged: {
            loser.sheetOpen = !alive
        }
        onWinnerChanged: {
            if(winner) {
                winBanner.highscorebeaten = (highscorehandler.highscore > gamearea.elapsed) || (highscorehandler.highscore === -1)
                if (winBanner.highscorebeaten) {
                    highscorehandler.highscore = gamearea.elapsed;
                }

            }
            win.sheetOpen = winner
        }
    }
}
