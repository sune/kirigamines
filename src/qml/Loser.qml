import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0

ColumnLayout {
    signal restartClicked()
    Label {
        Layout.fillWidth: true
        text: "You stepped on a bomb."
        wrapMode: TextArea.Wrap
    }
    Button {
        text: "Restart"
        onClicked: {
            restartClicked()
        }
    }
}
